
Visão Geral
Nesta avaliação, você irá construir um app usando os seguintes conceitos:

Estado
Event Handlers
Fetch
Renderização Condicional
Biblioteca de Componentes
Nas últimas avaliações, nós passamos uma estrutura de um projeto para você fazer o fork e clonar, porém agora você construirá este projeto do zero usando create-react-app.

Veja o Produto Final no final da página.

Início
Parte 1 - Criar um Botão com um Event Handler Básico

Crie um App usando o create-react-app
Execute seu app e visualize-o no navegador para se certificar que ele funciona
Limpe o conteúdo padrão do método render() do Componente App
Renderize um elemento botão em seu Componente App
Adicione uma função handleToggle ao Componente App
Adicione a função handleToggle ao evento de clique do seu botão
Recomendamos o handler console.log() para verificar se ele está funcionando corretamente
Neste ponto, você deve ter um botão simples que irá acionar um console.log quando clicado.

Parte 2 - Obter Dados da API do Github e Atualizar o Estado

Adicione um estado ao seu app com 2 chaves: user: {} e active: false
Em seu envent handler handleToggle, crie um fetch que irá realizar uma solicitação GET nesta url

https://api.github.com/users/a-github-username

Observação: certifique-se de substituir a-github-username por um nome de usuário de verdade

Atualize o estado com o resultado deste fetch

Neste ponto, quando você clica no botão, ele deve adicionar a informação do Github ao estado. (Use a Extensão para Chrome React Dev Tools para verificar)

Parte 3 - Renderizar a Informação do Usuário Github na Página

Use renderização condicional para exibir as informações do usuário Github a partir do estado quando o botão for clicado
Rederize Especificamente:
A imagem do seu perfil usando avatar_url
Seu nome
Pelo menos 2 outras informações do usuário Github.
Certifique-se de que, ao ser clicado, o botão alterna entre exibir e ocultar as informações do usuário.

dica: é aqui que o state.active pode ser útil
Neste ponto, a funcionalidade central do seu app deve se alinhar com o vídeo do produto final na parte inferior dessa página. Especificamente, um botão que alterna a exibição/ocultação de suas informações na página.

Parte 4 - Biblioteca de Componentes

Você precisa usar uma biblioteca de componentes. Você pode usar qualquer biblioteca de componentes, entretanto, ela deve ter componentes que você possa usar de fato nesta avaliação. As bibliotecas de componente abaixo são ótimas opções. Elas têm um componente chamado "Card" que você deveria considerar usar nesta avaliação. Além disso, "Button" é outro ótimo componente para usar nesta avaliação.

Semantic-UI-React -- observação: certifique-se de estar sempre usando o domínio react.semantic-ui.com. Há uma versão de ui semântica que usa apenas nomes de classes em semantic-ui.com que NÃO é o que queremos.
Material-UI
Envio

Você precisa enviar uma url de implementação que exibe uma versão do seu código fonte rodando ao vivo. Você também precisa incluir a url do seu repo no envio.

Happy hacking!!
Exemplo do Produto Final (este exemplo usa componentes Semantic-UI-React):

https://s3.us-east-2.amazonaws.com/files.kenzie.academy/frontend-q2/github-card.mp4

Bônus

Adicione uma nova seção à página que contenha um formulário com uma caixa de inserção. A caixa de inserção deve pedir um usuário Github. Depois do usuário digitar um nome de usuário Github e enviar o formulário, ele deve exibir um card (deve se parecer com o mesmo criado na Parte 3) com a informação do usuário. O formulário pode ser reenviado a qualquer momento com um novo nome de usuário e deve continuar exibindo o card mas com as informações do novo usuário. Observação: nesta funcionalidade bônus, você deve tentar reutilizar o jsx que já escreveu para o card na Parte 3. Você pode reutilizá-lo facilmente ao criar um componente nomeado contendo esse jsx. Um possível nome seria "GithubCard".