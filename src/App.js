import React, { Component } from "react";
import Button from "@material-ui/core/Button";

import "./App.css";

const GITHUB_USER_URL = (userId) => `https://api.github.com/users/${userId}`;

class App extends Component {
  state = { user: {}, active: 1, search: "" };

  setSearch = (text) => {
    const searchUser = text.target.value;
    this.setState({ search: searchUser });
  };

  handleToggle = () => {
    this.setState({ active: 2 });

    fetch(GITHUB_USER_URL(this.state.search))
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        return this.setState({ user: response, active: 3});
      })
  };

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.handleToggle()
    }
  }

  render() {
    if (this.state.active === 1) {
      return (

        <div className="App">
          <div id="gitHubGranBox">
            <div id="information">Crachá GitHub</div>
          </div>
          <div id="searchBox">
            <input type="text" placeholder="github user" onChange={this.setSearch} onKeyPress={this.handleKeyPress}/>
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleToggle()}
            >
              BUSCAR
            </Button>
          </div>
        </div>
      );
    } else if (this.state.active === 2) {
      return (
        <div className="App">
          <div id="gitHubGranBox">
            <div id="information">Carregando...</div>
          </div>
          <div id="searchBox">
            <input type="text" onChange={this.setSearch} />
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleToggle()}
            >
              BUSCAR
            </Button>
          </div>
        </div>
      );
    } else {
      const gitHubData = this.state.user
      return (
        <div className="App">
          <div id="gitHubGranBox">
            <div id="information">
              <div id="gitHubCard">
                <img
                  alt={gitHubData.login}
                  key={gitHubData.id}
                  src={gitHubData.avatar_url}
                />
                <div>{gitHubData.login}</div>
                <div className="information2">{gitHubData.company}</div>
                <div className="information2">{gitHubData.location}</div>
              </div>
            </div>
          </div>
          <div id="searchBox">
            <input type="text" onChange={this.setSearch} onKeyPress={this.handleKeyPress}/>
            <Button
              variant="contained"
              color="primary"
              onClick={() => this.handleToggle()}
            >
              BUSCAR
            </Button>
          </div>
        </div>
      );
    }
  }
}

export default App;
